## Movie

## Descrição
O objetivo é fornecer uma API que recuperar o maior e o menor intervalo das premiaçãos consegutivas dos produtores de filmes.
A API fornece a opção de carregar uma lista de filmes a parter de um arquivo no formato cvs.

## Pré requisitos
Para a API executar é preciso que o Java e o Maven estejam instalados

## Inicialização
Para executar a API, na pasta raiz do projeto, execute o comando `mvn clean spring-boot:run`

Para executar os testes da API, na pasta raiz do projeto, execute o comando mvn test

Para pré-carregar os filmes na inicialização da aplicação é preciso adicionalos no arquivo `movielist.csv` dentro da pasta `src/main/resources/csvs/` utilizando o formato "`year;title;studios;producers;winner`"

## Uso
Para utilizar a API basta fazer uma requisição GET para o endereço `<endereço>/movies-awards/intervals-between-awards`

Exempplo: localhot:8080/movies-awards/intervals-between-awards

