package com.classification.movies.advices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.classification.movies.exceptions.ModelDuplicatedException;
import com.classification.movies.exceptions.ModelNotFoundException;
import com.classification.movies.exceptions.NullModelException;

@ControllerAdvice
public class ExceptionAdvice {

	@ResponseBody
	@ExceptionHandler(ModelNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String ModelNotFoundHandler(ModelNotFoundException ex) {
		return ex.getMessage();
	}

	@ResponseBody
	@ExceptionHandler(ModelDuplicatedException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String ModelDuplicateHandler(ModelDuplicatedException ex) {
		return ex.getMessage();
	}
	
	@ResponseBody
	@ExceptionHandler(NullModelException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String NullModelHandler(NullModelException ex) {
		return ex.getMessage();
	}
}
