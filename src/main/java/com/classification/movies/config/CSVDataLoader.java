package com.classification.movies.config;

import java.io.File;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

public class CSVDataLoader {

	private static final Logger logger = LoggerFactory.getLogger(LoadDatabase.class);

	public static <T> List<T> loadObjectList(Class<T> clazz, String fileName) {
		try {
			CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader().withColumnSeparator(';').withColumnReordering(true).withNullValue("");
			
			CsvMapper mapper = new CsvMapper();
			
			File file = new ClassPathResource(fileName).getFile();
			MappingIterator<T> readValues = mapper.readerFor(clazz).with(bootstrapSchema).readValues(file);
			return readValues.readAll();
		} catch (Exception e) {
			logger.error("Error occurred while loading object list from file " + fileName, e);
			return Collections.emptyList();
		}
	}
}
