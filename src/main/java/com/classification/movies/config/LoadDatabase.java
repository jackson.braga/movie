package com.classification.movies.config;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.classification.movies.dtos.MovieCVSDTO;
import com.classification.movies.models.Movie;
import com.classification.movies.models.Producer;
import com.classification.movies.repositories.MovieRepository;

@Configuration
public class LoadDatabase {

	private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

	@Bean
	CommandLineRunner initDatabase(MovieRepository movieRepository) {
		return args -> {
			List<MovieCVSDTO> movieList = CSVDataLoader.loadObjectList(MovieCVSDTO.class, "csvs/movielist.csv");

			for (MovieCVSDTO mDTO : movieList) {
				Movie movie = convertToMovie(mDTO);
				if (movie != null) {
					movieRepository.save(movie);
				}
			}

			movieRepository.findAll().forEach(movie -> log.info("Preloaded: " + movie));
		};
	}

	private Movie convertToMovie(MovieCVSDTO mDTO) {
		Movie movie = new Movie(mDTO.getTitle(), mDTO.getStudios(), mDTO.getYear(), mDTO.getWinner());

		movie.setProducers(extractProducers(mDTO.getProducers()));

		return movie;
	}

	private Set<Producer> extractProducers(String producers) {
		Set<Producer> allProducers = new HashSet<Producer>();

		String[] splitComma = producers.split(",");

		for (int i = 0; i < splitComma.length; i++) {
			String splitedComma = splitComma[i];

			String[] splitAnd = splitedComma.split(" and ");

			for (int j = 0; j < splitAnd.length; j++) {
				String producer = splitAnd[j];
				if (!producer.isBlank()) {
					allProducers.add(new Producer(producer.trim()));
				}
			}
		}

		return allProducers;
	}
}