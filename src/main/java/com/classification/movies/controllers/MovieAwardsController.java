package com.classification.movies.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.classification.movies.projections.IMovieAwardProjection;
import com.classification.movies.services.MovieAwardsService;

@RestController
public class MovieAwardsController {

	private final MovieAwardsService service;
	
	@Autowired
	public MovieAwardsController(MovieAwardsService service) {
		this.service = service;
	}

	@GetMapping("/movies-awards/intervals-between-awards")
	public ResponseEntity<Map<String, List<IMovieAwardProjection>>> intervalsBetweenAwards() {
		Map<String, List<IMovieAwardProjection>> intervals = service.getIntervals();
		return ResponseEntity.ok(intervals);
	}
}
