package com.classification.movies.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.classification.movies.exceptions.ModelNotFoundException;
import com.classification.movies.models.Movie;
import com.classification.movies.services.MovieService;

@RestController
public class MovieController {

	private final MovieService movieService;
	
	@Autowired
	public MovieController(MovieService movieService) {
		this.movieService = movieService;
	}

	@GetMapping("/movies/{id}")
	public ResponseEntity<Movie> findById(@PathVariable Long id) {
		return movieService.findById(id).map(movie -> {
			return ResponseEntity.ok(movie);
		}).orElseThrow(() -> new ModelNotFoundException("Movie with id "+id+" not foutd"));
	}
	
	@PostMapping("/movies")
	public ResponseEntity<Movie> upsertMovie(@RequestBody Movie movie) {
		return ResponseEntity.status(HttpStatus.CREATED).body(movieService.upsert(movie));
	}
}
