package com.classification.movies.dtos;

import java.util.Objects;

public class MovieCVSDTO {

	private String title;
	private String studios;
	private String producers;
	private Integer year;

	private String winner;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public String getProducers() {
		return producers;
	}

	public void setProducers(String producers) {
		this.producers = producers;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.title, this.studios, this.producers, this.year, this.winner);
	}

	@Override
	public String toString() {
		return "MovieCSVDTO{title=" + this.title + ", studios=" + this.studios + ", producers=" + this.producers
				+ ", year=" + this.year + ", winner=" + this.winner + "}";
	}
}