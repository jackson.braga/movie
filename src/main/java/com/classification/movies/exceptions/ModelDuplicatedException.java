package com.classification.movies.exceptions;

public class ModelDuplicatedException extends RuntimeException {

	public ModelDuplicatedException() {
	}

	public ModelDuplicatedException(String message) {
		super(message);
	}

	public ModelDuplicatedException(Throwable cause) {
		super(cause);
	}

	public ModelDuplicatedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ModelDuplicatedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
