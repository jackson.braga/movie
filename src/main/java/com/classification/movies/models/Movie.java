package com.classification.movies.models;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Movie {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	private String studios;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(name = "movie_producer", 
			joinColumns = {
					@JoinColumn(name = "movie_id", referencedColumnName = "id", nullable = false, updatable = false) }, 
			inverseJoinColumns = {
					@JoinColumn(name = "producer_id", referencedColumnName = "id", nullable = false, updatable = false) })
	private Set<Producer> producers = new HashSet<Producer>();

	@Column(nullable = false)
	private Integer year;

	private String winner;

	public Movie() {
	}

	public Movie(String title, String studios, Integer year, String winner) {
		this.title = title;
		this.studios = studios;
		this.year = year;
		this.winner = winner;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStudios() {
		return studios;
	}

	public void setStudios(String studios) {
		this.studios = studios;
	}

	public Set<Producer> getProducers() {
		return producers;
	}

	public void setProducers(Set<Producer> producers) {
		this.producers = producers;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Movie)) {
			return false;
		}
		Movie movie = (Movie) obj;

		return this.id == movie.id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id, this.title, this.studios, this.year, this.winner);
	}

	@Override
	public String toString() {
		return "Movie{id=" + this.id + ", title='" + this.title + "', studios='" + this.studios + "', producers='"
				+ this.producers + "', year=" + this.year + ", winner=" + this.winner;
	}

}
