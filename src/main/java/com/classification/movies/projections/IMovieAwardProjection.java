package com.classification.movies.projections;

import org.springframework.beans.factory.annotation.Value;

public interface IMovieAwardProjection {

	public String getProducer();

	@Value("#{target.diff}")
	public Integer getInterval();

	public Integer getPreviousWin();

	public Integer getFollowingWin();
	
}
