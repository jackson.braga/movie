package com.classification.movies.repositories;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.classification.movies.models.Movie;
import com.classification.movies.projections.IMovieAwardProjection;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	@Query(value = "select "
			+ "m.* "
			+ "from movie m "
			+ "inner join movie_producer mp on (m.id = mp.movie_id) "
			+ "inner join producer p on (mp.producer_id = p.id) "
			+ "where m.title = :title "
			+ "and p.name in (:producers)", nativeQuery = true)
	Optional<Movie> findByTitleAndProducers(String title, Set<String> producers);

	@Query(value = "select "
			+ "p.name as producer, "
			+ "min(m.year) as previousWin, "
			+ "max(m.year) as followingWin, "
			+ "max(m.year) - min(m.year) as diff "
			+ "from movie m "
			+ "inner join movie_producer mp on (m.id = mp.movie_id) "
			+ "inner join producer p on (mp.producer_id = p.id) "
			+ "where m.winner = 'yes' "
			+ "group by p.name "
			+ "order by p.name", nativeQuery = true)
	List<IMovieAwardProjection> findMaxInterval();

	@Query(value = "select "
			+ "p.name as producer, "
			+ "lag(m.year) over (partition by p.name order by year) as previouswin, "
			+ "m.year as followingwin, "
			+ "m.year - coalesce(lag(m.year) over (partition by p.name order by m.year), m.year) as diff "
			+ "from movie m "
			+ "inner join movie_producer mp on (m.id = mp.movie_id) "
			+ "inner join producer p on (mp.producer_id = p.id) "
			+ "where m.winner = 'yes' "
			+ "order by p.name ", nativeQuery = true)
	List<IMovieAwardProjection> findMinInterval();
}
