package com.classification.movies.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.classification.movies.models.Producer;

public interface ProducerRepository extends JpaRepository<Producer, Long> {

	Optional<Producer> findByName(String name);

}
