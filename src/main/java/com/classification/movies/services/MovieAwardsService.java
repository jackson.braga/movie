package com.classification.movies.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classification.movies.projections.IMovieAwardProjection;
import com.classification.movies.repositories.MovieRepository;

@Service
@Transactional
public class MovieAwardsService {

	private final MovieRepository repository;

	@Autowired
	public MovieAwardsService(MovieRepository repository) {
		this.repository = repository;
	}

	public Map<String, List<IMovieAwardProjection>> getIntervals() {
		List<IMovieAwardProjection> mins = getMinIntervalsAwards();
		List<IMovieAwardProjection> maxs = getMaxIntervalsAwards();
		
		Integer minValue = getMinInterval(mins);
		Integer maxValue = getMaxInterval(maxs);

		HashMap<String, List<IMovieAwardProjection>> intervals = new HashMap<String, List<IMovieAwardProjection>>();
		intervals.put("min", mins.stream().filter(m -> m.getInterval() == minValue).collect(Collectors.toList()));
		intervals.put("max", maxs.stream().filter(m -> m.getInterval() == maxValue).collect(Collectors.toList()));
		return intervals;
	}

	private Integer getMaxInterval(List<IMovieAwardProjection> maxs) {
		Optional<IMovieAwardProjection> maxOp = maxs.stream().max((m1, m2) -> m1.getInterval().compareTo(m2.getInterval()));
		Integer maxValue = maxOp.isPresent() ? maxOp.get().getInterval() : 0;
		return maxValue;
	}

	private Integer getMinInterval(List<IMovieAwardProjection> mins) {
		Optional<IMovieAwardProjection> minOp = mins.stream().min((m1, m2) -> m1.getInterval().compareTo(m2.getInterval()));
		Integer minValue = minOp.isPresent() ? minOp.get().getInterval() : 0;
		return minValue;
	}

	public List<IMovieAwardProjection> getMinIntervalsAwards() {
		List<IMovieAwardProjection> movies = repository.findMinInterval();
		Map<String, IMovieAwardProjection> minIntervals = movies.stream().filter(movie -> movie.getInterval() > 0)
				.collect(Collectors.toMap(IMovieAwardProjection::getProducer, Function.identity(),
						(m1, m2) -> m1.getInterval() < m2.getInterval() ? m1 : m2));

		return minIntervals.values().stream().collect(Collectors.toList());
	}

	public List<IMovieAwardProjection> getMaxIntervalsAwards() {
		List<IMovieAwardProjection> movies = repository.findMaxInterval();
		return movies.stream().filter(movie -> movie.getInterval() > 0).collect(Collectors.toList());
	}
}
