package com.classification.movies.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.classification.movies.exceptions.ModelDuplicatedException;
import com.classification.movies.exceptions.NullModelException;
import com.classification.movies.models.Movie;
import com.classification.movies.models.Producer;
import com.classification.movies.repositories.MovieRepository;
import com.classification.movies.repositories.ProducerRepository;

@Service
@Transactional
public class MovieService {

	private final MovieRepository movieRepository;
	private final ProducerRepository produceRepository;

	@Autowired
	public MovieService(MovieRepository movieRepository, ProducerRepository produceRepository) {
		this.movieRepository = movieRepository;
		this.produceRepository = produceRepository;
	}

	public Movie upsert(Movie movie) {
		if (movie == null) {
			throw new NullModelException("Movie is null.");
		}

		Optional<Movie> movieOp = movieRepository.findByTitleAndProducers(movie.getTitle(), getProducersName(movie));

		if (movieOp.isPresent()) {
			throw new ModelDuplicatedException("Movie with Title " + movie.getTitle() + " and Producers "
					+ movie.getProducers() + " already exists.");
		}

		movie.setProducers(findProducers(movie.getProducers()));

		return movieRepository.save(movie);
	}

	private Set<String> getProducersName(Movie movie) {
		HashSet<String> names = new HashSet<String>();
		if(movie.getProducers() != null) {
			for (Producer producer : movie.getProducers()) {
				names.add(producer.getName());
			}
		}
		return names;
	}

	private Set<Producer> findProducers(Set<Producer> producers) {
		Set<Producer> newProducers = new HashSet<Producer>();

		if (producers == null) {
			return newProducers;
		}

		for (Producer producer : producers) {
			Optional<Producer> producerOp = produceRepository.findByName(producer.getName());
			if (producerOp.isPresent()) {
				newProducers.add(producerOp.get());
			} else {
				newProducers.add(producer);
			}
		}

		return newProducers;
	}

	public Optional<Movie> findById(Long id) {
		return movieRepository.findById(id);
	}

}
