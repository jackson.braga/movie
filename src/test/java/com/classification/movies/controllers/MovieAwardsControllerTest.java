package com.classification.movies.controllers;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class MovieAwardsControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void shouldReturnIntervalsBetweenAwardsSuccess() throws Exception {
		this.mockMvc
				.perform(MockMvcRequestBuilders.get("/movies-awards/intervals-between-awards")
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.min", CoreMatchers.notNullValue()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.min.length()", CoreMatchers.is(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.min[?(@.producer == 'Producer 1')].interval", CoreMatchers.hasItem(1)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.min[?(@.producer == 'Producer 3')].interval", CoreMatchers.hasItem(1)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.max", CoreMatchers.notNullValue()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.max.length()", CoreMatchers.is(2)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.max[?(@.producer == 'Producer 2')].interval", CoreMatchers.hasItem(54)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.max[?(@.producer == 'Producer 3')].interval", CoreMatchers.hasItem(54)));
	}
}
