package com.classification.movies.controllers;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.classification.movies.models.Movie;
import com.classification.movies.models.Producer;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class MovieControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Test
	public void shouldReturnNewMovie() throws Exception {
		Set<Producer> producers = Stream.of(new Producer("Charles Roven"), new Producer("Deborah Snyder")).collect(Collectors.toSet());
		Movie movie = new Movie("Batman v Superman: Dawn of Justice", "Warner Bros.", 2016, null);
		movie.setProducers(producers);
		
		this.mockMvc
			.perform(MockMvcRequestBuilders.post("/movies")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(movie)))
			.andExpect(MockMvcResultMatchers.status().isCreated())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.notNullValue()))
			.andExpect(MockMvcResultMatchers.jsonPath("$.title", CoreMatchers.is("Batman v Superman: Dawn of Justice")))
			.andExpect(MockMvcResultMatchers.jsonPath("$.studios", CoreMatchers.is("Warner Bros.")));
	}
	
	@Test
	public void shouldReturn400WhenCreateNewMovieDuplicated() throws Exception {
		Movie movie = new Movie("Title 1", "Studios.", 2016, null);
		movie.setProducers(Stream.of(new Producer("Producer 1")).collect(Collectors.toSet()));
		
		this.mockMvc
		
			.perform(MockMvcRequestBuilders.post("/movies")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(movie)))
			.andExpect(MockMvcResultMatchers.status().isBadRequest())
			.andExpect(MockMvcResultMatchers.header().string("Content-Type", CoreMatchers.is("text/plain;charset=UTF-8")))
			.andExpect(MockMvcResultMatchers.content().string(CoreMatchers.is("Movie with Title Title 1 and Producers [Producer{id=null, name=Producer 1}] already exists.")));
	}
	
	@Test
	public void sholdFetchOneMovieById() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get("/movies/{id}", 1))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(1)))
			.andExpect(MockMvcResultMatchers.jsonPath("$.title", CoreMatchers.is("Title 1")))
			.andExpect(MockMvcResultMatchers.jsonPath("$.producers.length()", CoreMatchers.is(1)))
			.andExpect(MockMvcResultMatchers.jsonPath("$.year", CoreMatchers.is(1980)))
			.andExpect(MockMvcResultMatchers.jsonPath("$.studios", CoreMatchers.is("Studios")));
		
	}
	
	@Test
	public void shouldReturn404WhenFindMovieById() throws Exception {
		Long id = 0L;

		this.mockMvc.perform(MockMvcRequestBuilders.get("/movies/{id}", id))
				.andExpect(MockMvcResultMatchers.status().isNotFound());
	}
}
